def add(a,b):
    c = a + b
    print(f"The sum of {a} and {b} is: {c}")

def subtract(a,b):
    c = a - b
    print(f"{a} - {b} = {c}")

def multiply(a,b):
    c = a * b
    print(f"{a} x {b} = {c}")

def divide(a, b):
    c = a / b
    print(f"{a}/{b} = {c}")